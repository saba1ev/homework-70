import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

export default class HomeScreen extends React.Component {
  state = {
    number: '',
    result: ''
  };

  getNumber = (number) =>{
    console.log(number);
    this.setState({
      number: this.state.number.concat(number)
    })
  };

  getResults = () => {
    this.setState({
      result: eval(this.state.number)
    })
  };

  clearAll = () =>{
    this.setState({
      number: ''
    })
  };

  clearOneNumber = () =>{
    const removeNumber = this.state.number.substr(0, this.state.number, -1);
    this.setState({
      number: removeNumber
    })
  }

  render() {
    const buttons = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', '*', '/', '=', 'C', '←'];
    return (
      <View>
        <View style={{marginTop: 30}}>
          <Text>{this.state.number}</Text>

        </View>
        <View>
          <Text>{this.state.result}</Text>
        </View>

        <View style={{flex: 1, flexDirection: 'row', width:250}}>
          {buttons.map((btn, index)=>{
            if (btn === '='){
              return(
                <TouchableOpacity key={index} style={{width: 40, height: 20, backgroundColor: 'blue'}} onPress={()=>this.getResults()}>
                  <View>
                    <Text>{btn}</Text>
                  </View>
                </TouchableOpacity>
              )
            } else if (btn === 'C') {
              return(
                <TouchableOpacity key={index} style={{width: 40, height: 20, backgroundColor: 'brown'}} onPress={()=> this.clearAll()}>
                  <View>
                    <Text>{btn}</Text>
                  </View>
                </TouchableOpacity>
              )
            } else if (btn === '←'){
              return(
                <TouchableOpacity key={index} style={{width: 40, height: 20, backgroundColor: 'brown'}} onPress={()=> this.clearOneNumber()}>
                  <View>
                    <Text>{btn}</Text>
                  </View>
                </TouchableOpacity>
              )
            } else {
              return(
                <TouchableOpacity key={index} style={{width: 40, height: 20, backgroundColor: 'brown'}} onPress={()=> this.getNumber(btn)}>
                  <View>
                    <Text>{btn}</Text>
                  </View>
                </TouchableOpacity>
              )
            }
          })}
        </View>
      </View>

    )
  }
}

